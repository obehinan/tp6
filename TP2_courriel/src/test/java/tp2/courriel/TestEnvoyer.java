package tp2.courriel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestEnvoyer {
	/*Classe de test de l'envoi d'un courriel*/
	
	private Courriel C, C_titre, C_PJ, C_adress ;

	
	@BeforeEach
	void setUp() throws BadAdressException {
		
		ArrayList<String> tabPJ = new ArrayList<String> ();
		ArrayList<String> tabPJ_empty = new ArrayList<String> ();
		tabPJ.add("voiture.png");
		this.C = new Courriel("none","none","none",tabPJ_empty) ;
		//Courriel correct
		C = new Courriel("SUJET","toto@gmail.com","Ceci est un message de TOTO avec une photo en pj",tabPJ);
		//courriel san titre
		C_titre = new Courriel("","toto@gmail.com","Ceci est un message de TOTO avec une photo en pj",tabPJ);
		//Courriel sans PJ
		C_PJ = new Courriel("TITRE","toto@gmail.com","Ceci est un message de TOTO avec une photo en pj",tabPJ_empty);
		//Courriel mauvais format adresse
		C_adress = new Courriel("SUJET","totogmail.com","Ceci est un message de TOTO avec une photo en pj",tabPJ);
	}

	 
	@Test
	void testString() {
		assertEquals("SUJET,toto@gmail.com,Ceci est un message de TOTO avec une photo en pj,voiture.png",C.toString());
	}
	

	@Test
	public void testBadAdresse() {
		assertThrows(BadAdressException.class, () -> {
			C_adress.checkAdresse();
		});
		
	}
	
	@Test
	public void testSansTitre() {
		assertThrows(TitreVideException.class, () -> {
			C_titre.checkTitre();
		});
	}
	

	@Test
	public void testSansPJ() {
		assertThrows(PJException.class, () -> {
			C_PJ.checkFullPJ();
		});
	}
}
