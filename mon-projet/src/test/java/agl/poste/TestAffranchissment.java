package agl.poste;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestAffranchissment {
	
	/*
	 * Classe de tests des affranchissement
	 */
	
	Colis colis1 ;
	Lettre lettre1, lettre2;
	
	@BeforeEach
	void setUp() {
		colis1 = new Colis("Le pere Noel", "famille Kaya, igloo 10, terres ouest","7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		lettre1 = new Lettre("Le pere Noel","famille Kirik, igloo 5, banquise nord","7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel","famille Kouk, igloo 2, banquise nord","5854", 18, 0.00018f, Recommandation.deux, true);
	}
	
	private static float tolerancePrix=0.001f;
	
	/* 0.5 tarif base
	 * 0.5 si taux recom = 1
	 * 1.5 si taux de recom = 2
	 * 0.30 si lettre urgente
	 */
	
	@Test
	void testAffranchissement1() {
		if(Math.abs(lettre1.tarifAffranchissement()-1.0f)<tolerancePrix){
			System.out.println("Affranchissement lettre 1 OK");
		} else{
			System.out.println("Affranchissement lettre 1 NOK");
		}
	}
	
	@Test
	void testAffranchissement2() {
		if(Math.abs(lettre2.tarifAffranchissement()-2.3f)<tolerancePrix){
			System.out.println("Affranchissement lettre 2 OK");
		} else{
			System.out.println("Affranchissement lettre 2 NOK");
		}
	}
	
	/* 2 tarif base
	 * 0.5 si taux recom = 1
	 * 1.5 si taux de recom = 2
	 * 3 si volume > 1/8 m3
	 */
	
	@Test
	void testAffranchissement3() {
		if(Math.abs(colis1.tarifAffranchissement()-3.5f)<tolerancePrix){
			System.out.println("Affranchissement colis 1 OK");
		} else{
			System.out.println("Affranchissement colis 1 NOK");
		}
	}
	
}
