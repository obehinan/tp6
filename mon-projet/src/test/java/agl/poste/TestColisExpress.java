package agl.poste;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestColisExpress {
	
	/*
	 * Classe de tests colis express
	 */
	Colis colis1 ;
	Lettre lettre1, lettre2;
	ColisExpress colisExpress1, colisExpress2 ;
	
	@BeforeEach
	void setUp() {
		colis1 = new Colis("Le pere Noel", "famille Kaya, igloo 10, terres ouest","7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		lettre1 = new Lettre("Le pere Noel","famille Kirik, igloo 5, banquise nord","7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel","famille Kouk, igloo 2, banquise nord","5854", 18, 0.00018f, Recommandation.deux, true);
		try {
			colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 25, 0.02f, Recommandation.deux, "train electrique", 200, true);
		} catch (ColisExpressInvalide e) {
			e.printStackTrace();
		}
	}
	
	private static float tolerancePrix=0.001f;
	/*
	 * test toString
	 */
	@Test
	 public void testToString() {
		if(colisExpress1.toString().equals("Colis express 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0/25.0/1")) {
			System.out.println("Colis express toString OK");}
		else {
			System.out.println("Colis express toString NOK");}
		}
	/*
	 * 30euros
	 * + 3euros si possède emballage
	 * c'est le cas ici
	 */
	@Test
	void testAffranchissement() {
		if(Math.abs(colisExpress1.tarifAffranchissement()-33.0f)<tolerancePrix){
			System.out.println("Colis express Affranchissement OK");
		} else{
			System.out.println("colis Express Affranchissement NOK");
		}
	}
	
	/*
	 * COLIS
	 * valeur 200
	 * Test remboursement des objets postaux
	 * 0% si taux de recommandation = 0
	 * 10% si taux de recommandation = 1
	 * 50% si taux de recommandation = 2
	 * taux recom = 2
	 * 
	 */
	@Test
	void testRemboursement() {
		if(Math.abs(colisExpress1.tarifRemboursement()-100.0f)<tolerancePrix){
			System.out.println("Colis express Remboursement OK");
		} else{
			System.out.println("colis Express Remboursement NOK");
		}
	}

}
