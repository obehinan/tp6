package tp2.courriel;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class TestEnvoyer {
	/*Classe de test de l'envoi d'un courriel*/
	
	//INITIALISATION
	private Courriel C ;
	
	@BeforeEach
	void setUp() throws BadAdressException {
		ArrayList<String> tabPJ_empty = new ArrayList<String> ();
		this.C = new Courriel("none","none","none",tabPJ_empty) ;
	}
	
	/* Méthode test paramétré avec source adresses incorrectes*/
	@ParameterizedTest
		@ValueSource(strings = {"adress", "toto.com"} )
		 void testAdresses(String name) {
			 C.setAdresse(name);
			 assertThrows(BadAdressException.class, () -> {C.checkAdresse();});
		 }
	
	/* Méthode test paramétré avec source aucun titre*/
	@ParameterizedTest
		@ValueSource(strings = {""} )
		 void testTitre(String name) {
			 C.setTitre(name);
			 assertThrows(TitreVideException.class, () -> {C.checkTitre();});
		 }
	
	
	/* Méthode test paramétré avec paramètre adresse*/
	 @Parameterized.Parameters
	    public static Stream<Arguments> badAdress() {
	        return Stream.of(
	            Arguments.of("adress", "manque '.@domaine' "),
	            Arguments.of("@domaine.fr", "manque début")
	        );
	    }
	 
	 @DisplayName("Mauvaises Adresses + raisons")
		 @ParameterizedTest(name = "Adresse : {0}, fail : {1}")
		    @MethodSource("badAdress")
		    public void testAdresses(String adress, String problem) {
		        C.setAdresse(adress);
		        assertThrows(BadAdressException.class, () -> {C.checkAdresse();});
		    }

}
