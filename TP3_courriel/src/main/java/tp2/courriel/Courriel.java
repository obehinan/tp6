package tp2.courriel;

import java.util.ArrayList;

public class Courriel {
	private String titre ;
	private String adresse ;
	private String message ;
	private ArrayList<String> PJ = new ArrayList<String>() ;
	
	
	//CONSTRUCTEUR
	public Courriel(String t, String a, String m, ArrayList<String> PJ) throws BadAdressException{
		this.titre = t ;
		this.adresse = a ;
		this.message = m ;
		this.PJ = PJ ;
	};	
	
	//GETTERS & SETTERS
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public ArrayList<String> getPJ() {
		return PJ;
	}

	public void setPJ(ArrayList<String> pJ) {
		PJ = pJ;
	}
	
	//METHODES DE CHECKING
	
	boolean checkTitre() throws TitreVideException {
		boolean res = (this.titre.length()!=0);
		if(!res) {
			throw new TitreVideException("Pas de titre");
		}
		return res;
	}
	
	
	boolean checkAdresse() throws BadAdressException {
		boolean res = this.adresse.matches("[a-zA-Z](.*)@([a-zA-Z]+)[\\.]([a-zA-Z]+)");
		if(!res) {
			throw new BadAdressException("Mauvaise adresse");
		}
		return res;
	}
	
	boolean checkMessagePJ() {
		return this.message.matches("(.*)[pP][jJ](.*)") || this.message.matches("(.*)[jJ]oint([e]?)");
	}
	
	boolean checkPJ() {
		return (this.PJ.size()>0);
	}
	
	boolean checkFullPJ() throws PJException {
		boolean res = this.checkMessagePJ() && this.checkPJ() ;
		if(!res) {
			throw new PJException("Pas de pièce jointe");
		}
		return res ;	
	}
	
	public String toString() {
		String res = this.titre+","+this.adresse+","+this.message+","; ;
		for(int i=0 ; i< this.PJ.size(); i++) {
			res += this.PJ.get(i) ;
			if(i!=this.PJ.size()-1) {
				res += "," ;
			}
		}
		return res ;
	}
	
}
