package fr.umfds.ter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class testTER{
	Sujet s1;
	Etudiant etu1;
	Etudiant etu2 ;
	Etudiant etu3 ;
	Etudiant etu4 ;
	Etudiant etu5 ;
	Etudiant etu6 ;
	ArrayList<Etudiant> membres = new ArrayList<Etudiant> (5);
	ArrayList<Etudiant> membres_test_effectif = new ArrayList<Etudiant> (6);
	Enseignant encadrant ;
	Enseignant rapporteur ;
	Groupe g;
	Soutenance sout1;
	@BeforeEach
	 void setUp() throws effectifInvalideException, GroupeException, SujetException{
		//rapporteur = new Enseignant("Slavoj", "Zizek");
		encadrant = new Enseignant("Abdel-Malek", "Bennabi");
		s1=new Sujet(1);
		etu1 = new Etudiant("Thorfinn", "Karlsefni");
		etu2 = new Etudiant("Jean ", "de Carrouges");
		etu3 = new Etudiant("Jacques", "Le Gris");  
		etu4 = new Etudiant("Marcus", "Aurelius");
		etu5 = new Etudiant("Abu", "Sina");
		etu6 = new Etudiant("Jean-Luc", "Goddard");
		membres.add(etu1);
		membres.add(etu2);
		membres.add(etu3);
		g = new Groupe(membres, encadrant/*, rapporteur*/);
	}
	
	@Test
	void testSoutenanceConstructeurCrenauxIncorrect() {
		//lambda expression require 1.8 version 
		assertThrows(CrenauxInvalideException.class, () ->{
			new Soutenance(19);
		});
	}
	
	@Test
	void testSoutenanceConstructeur2CrenauxIncorrect() {
		assertThrows(CrenauxInvalideException.class, () ->{
			new Soutenance(g, 19);
		});
		assertThrows(CrenauxInvalideException.class, () ->{
			new Soutenance(19);
		});
		Soutenance.creneauxReserves.clear();
	}
	
	@Test 
	void testSoutenanceConstructeurCrenauxPris() throws CrenauxInvalideException {
		Soutenance sout14 = new Soutenance(14);
		assertThrows(CrenauxInvalideException.class, () ->{
			new Soutenance(14);
		});
		assertThrows(CrenauxInvalideException.class, () ->{
			new Soutenance(g, 14);
		});
		Soutenance.creneauxReserves.clear();
	}
	
	@Test
	void testSoutenanceGetGroupe() throws CrenauxInvalideException {
		Soutenance.creneauxReserves.clear();
		sout1 = new Soutenance(14);
		assertThrows(GroupeException.class, ()->{
			sout1.getGroupe();
		});
		Soutenance.creneauxReserves.clear();
	}
	
	@Test
	void testSoutennceSetGroupe() throws GroupeException, CrenauxInvalideException, effectifInvalideException, SujetException {
		Soutenance.creneauxReserves.clear();
		sout1 = new Soutenance(14);
		sout1.setGroupe(g);
		assertTrue(sout1.getGroupe()==g);
		assertTrue(Soutenance.groupesAffectes.contains(g));
		ArrayList<Etudiant> membres_groupe_non_null = new ArrayList<>(3);
		membres_groupe_non_null.add(etu1);
		membres_groupe_non_null.add(etu2);
		membres_groupe_non_null.add(etu3);
		Groupe g2 = new Groupe(membres_groupe_non_null, encadrant/*, rapporteur*/);
		sout1.setGroupe(g2);
		assertTrue(sout1.getGroupe()==g2);
		assertTrue(Soutenance.groupesAffectes.contains(g2));
		assertTrue(!Soutenance.groupesAffectes.contains(g));
		Soutenance.creneauxReserves.clear();
		
	}

	@Test 
	void testSoutenanceSetEncadrant(){
		
	}
	
	
	@Test
	void testEnseignantEncadre() {
		//assertTrue(g.getEncadrant() == encadrant);
		assertTrue(encadrant.encadre(g));
	}

	@Test 
	void testEnseignantRapporte() throws GroupeException, effectifInvalideException{
		Enseignant rapporteur= new Enseignant("xx", "yy");
		rapporteur.ajoutSujet(s1);//1gr, 0suj
		assertTrue(rapporteur.rapporte(s1));//
	}
//AFFECTATION RAPPORTEUR
		
			
			
	//cas ou groupe est null
		@Test
		void testAffectationRapporteurGroupeNull() throws CrenauxInvalideException {
			sout1 = new Soutenance(14);
			//g setUp()
			assertThrows(GroupeException.class, () ->{
				sout1.setRapporteur(rapporteur);
			});
			Soutenance.creneauxReserves.clear();
		}
	
	//cas ou le rapporteur est l'encadrant	
	@Test
	void testAffectationRapporteurEstEncadrant() throws GroupeException, effectifInvalideException, SujetException, CrenauxInvalideException{
		sout1 = new Soutenance(14);
		g.setSujet(s1);
		sout1.setGroupe(g);
		assertThrows(EnseignantException.class, () ->{
			sout1.setRapporteur(encadrant);
		});
		//assertTrue(rapporteur.rapporte(s1));
		//assertTrue(s1.getRapporteur() == rapporteur);
		Soutenance.creneauxReserves.clear();
	}
		
	//cas ou le rapporteur a autant de sujets que de groupes
	@Test
	void testAffectationRapporteurPlusDeSujets() throws GroupeException, effectifInvalideException, SujetException, CrenauxInvalideException{
		sout1 = new Soutenance(14);
		//Enseignant zizek = new Enseignant("Slavoj", "zizek");//0gr, 0s
		g.setSujet(s1);
		sout1.setGroupe(g);
		rapporteur = new Enseignant("xx", "yy");
		assertThrows(effectifInvalideException.class, () ->{
			sout1.setRapporteur(rapporteur);//rapporteur = 0gr,0suj
		});
		//assertTrue(rapporteur.rapporte(s1));
		//assertTrue(s1.getRapporteur() == rapporteur);
		Soutenance.creneauxReserves.clear();
	}
	
	//cas ou tout marche
	@Test
	void testAffectationRapporteurOK() throws CrenauxInvalideException, GroupeException, effectifInvalideException, SujetException, EnseignantException {
		g.setSujet(s1);  //encadrant: 1gr, 0sujets(contrainte encadrant != rapporteur)
		sout1 = new Soutenance(g, 14);
		rapporteur = new Enseignant("xx", "yy");//rapporteur : 0gr, 0sujets
		Groupe g1 = new Groupe(membres, rapporteur);//rapporteur : 1gr, 0sujets
		sout1.setRapporteur(rapporteur);
		assertTrue(sout1.getRapporteur()==rapporteur);
		assertTrue(g.getRapporteur()==rapporteur);
		assertTrue(s1.getRapporteur()==rapporteur);
		assertTrue(rapporteur.getSujets().contains(s1));
	}
	
	@Test
	void testGroupeEffectifInvalide() {
		membres_test_effectif.add(etu1);
		membres_test_effectif.add(etu2);
		membres_test_effectif.add(etu3);
		membres_test_effectif.add(etu4);
		membres_test_effectif.add(etu5);
		membres_test_effectif.add(etu6);
		
		assertThrows(effectifInvalideException.class, () ->{
			new Groupe(membres_test_effectif, encadrant/*, rapporteur*/);
		});
	}
	
	@Test 
	void testAjouterVoeux() throws SujetException{
		g.ajouterVoeux(s1);
		assertTrue(g.getVoeux().contains(s1));
		///contrainte pas plus de 6 voeux
		Sujet s2 = new Sujet(2);
		g.ajouterVoeux(s2);
		
		Sujet s3 = new Sujet(3);
		g.ajouterVoeux(s3);
		
		Sujet s4 = new Sujet(4);
		g.ajouterVoeux(s4);
		
		Sujet s5 = new Sujet(5);
		g.ajouterVoeux(s5);
		
		Sujet s6 = new Sujet(6);
		g.ajouterVoeux(s6);
		
		Sujet s7 = new Sujet(7);
		assertThrows(SujetException.class, ()->{
			g.ajouterVoeux(s7);
		});
		
	}
	
	
	/*@Test 
	void testSoutenance1SetRapporteur() throws GroupeException, effectifInvalideException, SujetException, EnseignantException {
		g.setRapporteur(rapporteur);
		assertTrue(g.getRapporteur()==rapporteur);
		assertTrue(rapporteur.getGroupes().contains(g));
		assertThrows(GroupeException.class, ()->{
			g.setRapporteur(encadrant);
		});
	}*/
	
	@Test
	void testGroupeSetSujet() throws GroupeException, effectifInvalideException, SujetException {
		g.setSujet(s1);
		assertTrue(g.getSujet()==s1);
		assertTrue(s1.getGroupe()==g);	
		assertThrows(SujetException.class, ()->{
			g.setSujet(s1);
		});
	}
	
	@Test
	void testGroupeGetRapporteur() throws GroupeException, effectifInvalideException, SujetException, EnseignantException, CrenauxInvalideException {
		Soutenance.creneauxReserves.clear();
		assertThrows(EnseignantException.class, ()->{
			g.getRapporteur();//rapporteur == null
		});
		g.setSujet(s1);
		sout1 = new Soutenance(g,15);
		rapporteur = new Enseignant("xx", "yy");
		Groupe g1 = new Groupe(membres, rapporteur);
		sout1.setRapporteur(rapporteur);
		assertTrue(rapporteur == g.getRapporteur());
		Soutenance.creneauxReserves.clear();
	}
	
	@Test 
	void testGroupeGetMembres() {
		ArrayList<Etudiant> res = g.getMembres();
		assertTrue(res.contains(etu1) && res.contains(etu1) && res.contains(etu1) && res.contains(etu1) && res.size()==3);
	}
	
	
	@Test
	void testSujetToString() {
		assertEquals(s1.toString(),"Sujet={id:1}");
	}
	
/*	
	@Test 
	void testlistget() {
		assertTrue(etu1 == membres.get(0));
	}
*/
}
