package exo1;

import java.time.Clock;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

public class BibliUtilities {
	
	private InterfaceGlobalBibliographyAcess myInterface ;
	private Bibliothèque biblio = Bibliothèque.getInstance();
	private Clock horloge ;
	
	public BibliUtilities() {} ;
	
	public void setBiblio(Bibliothèque b) {
		this.biblio = b;
	}
	
	public ArrayList<NoticeBibliographique> chercherNoticesConnexes(NoticeBibliographique ref){
		/* Récupère toutes les Notices du même auteur */
		ArrayList<NoticeBibliographique> notices = myInterface.noticesDuMemeAuteurQue(ref);
		ArrayList<NoticeBibliographique> res = new ArrayList<NoticeBibliographique> () ;
		
		/* Ajout Notices titre != titre de ref */
		for(int i=0 ; i< notices.size() && res.size() <5 ; i++) {
			if(notices.get(i).getTitre()!=ref.getTitre()) {
				res.add(notices.get(i));
			}
		}
		/*On retitre toutes les notices au même titre*/
		for(int i=0 ; i<5 ; i++) {
			int j = 0 ;
			while(j<5 && res.size()<5 && notices.size()<5) {
				if(res.get(i).getTitre()==res.get(j).getTitre() && i!=j) {
					res.remove(i) ; 
				}
				j++;
			}
		}
		return res ;
	}
	
	public NoticeStatus ajoutNotice(String isbn) throws AjoutImpossibleException {
		NoticeBibliographique res ;
		try {
			res = myInterface.getNoticeFromIsbn(isbn);
		} catch (IncorrectIsbnException e) {
			throw new AjoutImpossibleException();
		}		
		return this.biblio.addNotice(res);
		
	}
	
	void prevoirInventaire() {
		LocalDate last = this.biblio.getLastInventaire() ;
		LocalDate now = LocalDate.now(horloge) ;
		Period period = Period.between(last, now);
		
		if(period.getMonths()>12 || period.getYears()>0) {
			this.biblio.inventaire();
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
