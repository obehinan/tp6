package exo1;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;



@ExtendWith(MockitoExtension.class)
public class TestBibliUtilities {
	
	private NoticeBibliographique n1;
	private NoticeBibliographique n2;
	private NoticeBibliographique n3;
	private NoticeBibliographique n4;
	private NoticeBibliographique n5;
	private NoticeBibliographique n6;
	private NoticeBibliographique n7;
	private NoticeBibliographique n8;
	private ArrayList<NoticeBibliographique> listeNotices;
	private ArrayList<NoticeBibliographique> res;
	private Bibliothèque biblio;
	
	@Mock
	Clock mockedClock;
	
	@Mock
	InterfaceGlobalBibliographyAcess glob;
	
	@InjectMocks
	BibliUtilities bib = new BibliUtilities();
	
	@BeforeEach
	void setup() {
		n1 = new NoticeBibliographique("0", "Harry Potter 1", "J.K. Rowling");
		n2 = new NoticeBibliographique("1", "Harry Potter 2", "J.K. Rowling");
		n3 = new NoticeBibliographique("2", "Harry Potter 3", "J.K. Rowling");
		n4 = new NoticeBibliographique("3", "Harry Potter 4", "J.K. Rowling");
		n5 = new NoticeBibliographique("4", "Harry Potter 5", "J.K. Rowling");
		n6 = new NoticeBibliographique("5", "A Song of Ice and Fire", "Georges R. R. Martin");
		n7 = new NoticeBibliographique("6", "The Winds of WInter", "Georges R. R. Martin");
		n8 = new NoticeBibliographique("8", "Survivant", "Chuck Palahniuk");
		listeNotices = new ArrayList<NoticeBibliographique>();
		res = new ArrayList<NoticeBibliographique>();
		biblio = Bibliothèque.getInstance();
		biblio.addNotice(n1);
		biblio.addNotice(n5);
	}
	
	@Test
	public void testConnexe5Titres() {
		/*Résultat de noticesDuMemeAuteurQue(n1)*/
		listeNotices.add(n1);
		listeNotices.add(n2);
		listeNotices.add(n3);
		listeNotices.add(n4);
		listeNotices.add(n5);
		/*Résultat de chercherNoticesConnexes(n1)*/
		res.add(n2);
		res.add(n3);
		res.add(n4);
		res.add(n5);
		when(glob.noticesDuMemeAuteurQue(n1)).thenReturn(listeNotices);
		assertEquals(res.size(),bib.chercherNoticesConnexes(n1).size());
	}
	
	@Test
	public void testAjoutNotice() throws IncorrectIsbnException {
		when(glob.getNoticeFromIsbn("9999999")).thenThrow(IncorrectIsbnException.class);
		assertThrows(AjoutImpossibleException.class, () -> {
			bib.ajoutNotice("9999999");});
	}
	
	@Test
	public void testPrevoirInventairePasBesoin() {
		LocalDate uneDate = LocalDate.of(2021, 01, 01);
		Clock fixedClock = Clock.fixed(uneDate.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		LocalDate last  = biblio.getLastInventaire();
		
		bib.prevoirInventaire();
		LocalDate nouveauInv = biblio.getLastInventaire();
		
		assertEquals(nouveauInv,last);
	}
	
	@Test
	public void testPrevoirInventaireBesoin() {
		LocalDate uneDate = LocalDate.of(2022, 01, 01);
		Clock fixedClock = Clock.fixed(uneDate.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		LocalDate last  = biblio.getLastInventaire();
		
		bib.prevoirInventaire();
		LocalDate nouveauInv = biblio.getLastInventaire();
		
		assertNotEquals(nouveauInv,last);
		}
}
