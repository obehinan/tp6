package agl.poste;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestSacPostaux {
	/*
	 * Classe de tests des sacs Postaux
	 */
	Colis colis1 ;
	Lettre lettre1, lettre2;
	SacPostal sac1;
	
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	/*
	 * Initialisation
	 */
	@BeforeEach
	void setUp() {
		colis1 = new Colis("Le pere Noel", "famille Kaya, igloo 10, terres ouest","7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		lettre1 = new Lettre("Le pere Noel","famille Kirik, igloo 5, banquise nord","7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel","famille Kouk, igloo 2, banquise nord","5854", 18, 0.00018f, Recommandation.deux, true);
		sac1 = new SacPostal();
		sac1.ajoute(lettre1);
		sac1.ajoute(lettre2);
		sac1.ajoute(colis1);
	}
	
	@Test
	void testRemboursement() {
		assertTrue(Math.abs(sac1.valeurRemboursement()-116.5f)<tolerancePrix);
	}
	
	@Test
	void testVolume() {
		assertTrue(Math.abs(sac1.getVolume()-0.025359999558422715f)<toleranceVolume);
	}
	
	@Test
	void testVolume2() {
		SacPostal sac2 = sac1.extraireV1("7877");
		assertTrue(Math.abs(sac2.getVolume()-0.02517999955569394f)<toleranceVolume);
	}

}
