package agl.poste;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestRemboursement {
	
	Colis colis1 ;
	Lettre lettre1, lettre2;
	
	@BeforeEach
	void setUp() {
		colis1 = new Colis("Le pere Noel", "famille Kaya, igloo 10, terres ouest","7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		lettre1 = new Lettre("Le pere Noel","famille Kirik, igloo 5, banquise nord","7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel","famille Kouk, igloo 2, banquise nord","5854", 18, 0.00018f, Recommandation.deux, true);
	}
	
	/*
	 * LETTRES
	 * Test remboursement des objets postaux
	 * 0 si taux de recommandation = 0
	 * 1.5 si taux de recommandation = 1
	 * 15 si taux de recommandation = 2
	 */
	
	private static float tolerancePrix=0.001f;
	
	@Test
	void testRemboursement1() {
		 assertTrue(Math.abs(lettre1.tarifRemboursement()-1.5f)<tolerancePrix);
	}
	
	@Test
	void testRemboursement2() {
		assertTrue(Math.abs(lettre2.tarifRemboursement()-15.0f)<tolerancePrix);
	}
	
	/*
	 * COLIS
	 * Test remboursement des objets postaux
	 * 0% si taux de recommandation = 0
	 * 10% si taux de recommandation = 1
	 * 50% si taux de recommandation = 2
	 */
	@Test
	void testRemboursement3() {
		assertTrue(Math.abs(colis1.tarifRemboursement()-100.0f)<tolerancePrix);
	}
	
}
