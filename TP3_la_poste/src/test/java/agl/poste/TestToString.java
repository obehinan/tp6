package agl.poste;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestToString {
	/*
	 * Classe de tests toString des objets postaux
	 */
	
	
	Colis colis1 ;
	Lettre lettre1, lettre2;
	/*
	 * Initialisation des objets à tester
	 */
	@BeforeEach
	void setUp() {
		colis1 = new Colis("Le pere Noel", "famille Kaya, igloo 10, terres ouest","7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		lettre1 = new Lettre("Le pere Noel","famille Kirik, igloo 5, banquise nord","7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel","famille Kouk, igloo 2, banquise nord","5854", 18, 0.00018f, Recommandation.deux, true);
	}
	
	@Test
	public void testColis()
	{
		assertEquals("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0", colis1.toString());
	}

	@Test
	void testLettre1() {
		assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire", lettre1.toString());
	}
	
	@Test
	void testLettre2() {
		assertEquals("Lettre 5854/famille Kouk, igloo 2, banquise nord/2/urgence", lettre2.toString());	
	}
}
