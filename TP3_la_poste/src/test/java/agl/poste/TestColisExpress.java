package agl.poste;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestColisExpress {
	
	/*
	 * Classe de tests colis express
	 */
	Colis colis1 ;
	Lettre lettre1, lettre2;
	ColisExpress colisExpress1, colisExpress2 ;
	
	@BeforeEach
	void setUp() {
		colis1 = new Colis("Le pere Noel", "famille Kaya, igloo 10, terres ouest","7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
		lettre1 = new Lettre("Le pere Noel","famille Kirik, igloo 5, banquise nord","7877", 25, 0.00018f, Recommandation.un, false);
		lettre2 = new Lettre("Le pere Noel","famille Kouk, igloo 2, banquise nord","5854", 18, 0.00018f, Recommandation.deux, true);
		try {
			colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 25, 0.02f, Recommandation.deux, "train electrique", 200, true);
		} catch (ColisExpressInvalide e) {
			System.out.println(e.getMessage());
		}
	}
	
	private static float tolerancePrix=0.001f;

	@Test
	 public void testToString() {
		assertEquals("Colis express 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0/25.0/1", colisExpress1.toString());
		}
	/*
	 * 30euros
	 * + 3euros si possède emballage
	 * c'est le cas ici
	 */
	@Test
	void testAffranchissement() {
		assertTrue(Math.abs(colisExpress1.tarifAffranchissement()-33.0f)<tolerancePrix);
	}
	
	/*
	 * COLIS
	 * valeur 200
	 * Test remboursement des objets postaux
	 * 0% si taux de recommandation = 0
	 * 10% si taux de recommandation = 1
	 * 50% si taux de recommandation = 2
	 * taux recom = 2
	 * 
	 */
	@Test
	void testRemboursement() {
		assertTrue(Math.abs(colisExpress1.tarifRemboursement()-100.0f)<tolerancePrix);
	}
	
	@Test
	void colisSup30KG() throws ColisExpressInvalide {
		try {
		ColisExpress c = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 40, 0.02f, Recommandation.deux, "train electrique", 200, true);	 
		 } catch (Exception e ) {
			 System.out.println(e.getMessage());
		 }
	}

}
